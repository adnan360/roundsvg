# roundsvg

Tests ways to fix a randomly placed object causing anti-aliasing issues in the Inkscape export.

## Tests

The `test.svg` has been created in Inkscape, dimensions set to 10x10px, units set to px and grids setup to 1x1px (but grid snapping has been disabled). Then a rectangle has been placed at random place. The file is saved as `test.svg` and exported. Then a scaled version has been prepared with `../scale.sh`. The export and scaling can be automated by running `./export.sh` on each directory.

| Directory               | Description                                                    | Export (scaled)  |
| ----------------------- | -------------------------------------------------------------- |:----------------:|
| `01-original`           | has the original file as mentioned above.                      | ![01-original](https://codeberg.org/adnan360/roundsvg/raw/branch/main/01-original/export-scaled.png) |
| `02-no-antialiasing`    | has the **File - Document properties - Use antialiasing** unchecked | ![02-no-antialiasing](https://codeberg.org/adnan360/roundsvg/raw/branch/main/02-no-antialiasing/export-scaled.png) |
| `03-snap-enabled`       | has the file with grids visible and "edge midpoints" snapping enabled. A new rectangle with the same styling has been drawn (since there is no other way to test snapping). As 1x1px grid was setup before, it snaps to the grid but only shape outline is snapped, not the border. So the issue is not fixed. | ![03-snap-enabled](https://codeberg.org/adnan360/roundsvg/raw/branch/main/03-snap-enabled/export-scaled.png) |
| `04-manual-positioning` | has the rectangle object positioned manually so that the border is aligned. However, the size has not been changed. So it is showing the issue on right and bottom edges. As a note, this can be fixed manually by resizing the object, but left this way to demonstrate how it would look when the designer has less time ;) ... or didn't notice the issue. | ![04-manual-positioning](https://codeberg.org/adnan360/roundsvg/raw/branch/main/04-manual-positioning/export-scaled.png) |
| `05-script`             | has a script named `roundsvg.pl` which is called by `export.sh` that automatically positions the object to fix the issue. | ![05-script](https://codeberg.org/adnan360/roundsvg/raw/branch/main/05-script/export-scaled.png) |

## Scripts

Some scripts that make this easier. Please post an issue if you want more details.

### `export.sh`

Usually exports an `export.png` from Inkscape `test.svg` in the current directory and prepares a scaled version on `export-scaled.png`. It can be run without running Inkscape manually, so can be considered as an automated way to produce outputs. In some cases it can manipulate the .svg file (e.g. it does so in case of `05-script`).

```sh
cd 01-original
./export.sh
```

### `roundsvg.pl`

A Perl script to round dimensions (`x`, `y`, `width`, `height`). It is still a proof of concept and not tested thoroughly. Feel free to [post an issue](https://codeberg.org/adnan360/roundsvg/issues) if you see any problems.

```sh
./roundsvg.pl input.svg output.svg
```

### `scale.sh`

Scales `export.png` on current directory to 300% and saves as `export-scaled.png`. It is not needed in most cases and `export.sh` should be enough.

```sh
cd 01-original
../scale.sh
```

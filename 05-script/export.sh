#!/usr/bin/env sh

cd "$(dirname $0)"
# round position values
./roundsvg.pl test.original.svg test.svg
# export command
inkscape --export-filename=export.png test.svg
# scale exported image
../scale.sh

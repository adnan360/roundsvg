#!/usr/bin/env perl

package roundsvg;

use warnings;
use strict;

sub get_file_contents {
	my $filepath = shift;
	if ( -f $filepath ) {
		open( my $file_handle, "<", "$filepath" ) or die("File $filepath not found");
		$/ = undef;
		my $content = <$file_handle>;
		close($file_handle);
		return $content;
	} else {
		return '';
	}
}

sub write_file_contents {
	my $string = shift;
	my $filepath = shift;

	open(my $file_handler, '>', $filepath) or die $!;
	print $file_handler $string;
	close($file_handler);
}

my $scale_x=0.26458;
my $scale_y=0.26458;
# TODO: Extract from object (now it will work perfectly only with objects that have 1px stroke)
my $stroke_width=1;

sub round_number {
	# The value for the dimension
	my $float = shift;
	# Name of the dimension - x, y, width or height
	my $dimension = shift;
	# Either will contain $scale_x or $scale_y
	my $scale_val;
	$float = $float / $scale_x;
	$float = int($float + $float/abs($float*2 || 1));
	if ( $dimension eq 'x' or $dimension eq 'width' ) {
		$scale_val = $scale_x;
	} elsif ( $dimension eq 'height' or $dimension eq 'y' ) {
		$scale_val = $scale_y;
	}
	if ( $dimension eq 'x' or $dimension eq 'y' ) {
		return ($float * $scale_val) + (($stroke_width * $scale_val) / 2);
	} else {
		return ($float * $scale_val);
	}
}

sub round_svg {
	my $content = get_file_contents(shift);

	$content =~ /viewBox="(.*) (.*) (.*) (.*)"/;
	my $scale_x=$3;
	my $scale_y=$4;

	$content =~ s/\s(width|height|x|y)="([+-]?([0-9]*[.])?[0-9]+)"/' '.$1.'="'.round_number($2, $1).'"'/ge;

	write_file_contents($content, shift);
}


if ( -f $ARGV[0] ) {
	round_svg($ARGV[0], $ARGV[1]);
} else {
	print "File '${ARGV[0]}' is not found\n"
}

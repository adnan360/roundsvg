#!/usr/bin/env sh

if [ -z "$(command -v convert)" ]; then
	echo 'Requires ImageMagick to be installed'
	exit 1
fi

convert export.png -scale 300% export-scaled.png

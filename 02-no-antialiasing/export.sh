#!/usr/bin/env sh

cd "$(dirname $0)"
# export command
inkscape --export-filename=export.png test.svg
# scale exported image
../scale.sh
